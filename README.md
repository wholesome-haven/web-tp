# web-tp
Our goal is to write software to accuratly simulate the behaviour of an toy-processor.
Our processor is based on a design taught at the University Tübingen with some changes.
We plan to implement our project in go with web-interface in go-app. This would enable hosting the go-app server anywhere and running the application client side via web-assembly.

# Goals
We are just starting to collect our ideas in the wiki. 
- [ ] Logisim 
    - We want to make our processor work irl, that why we are starting to test basic functionallity in ligisim evolution first
- [ ] Backend
- [ ] Frontend
